import { configureStore } from '@reduxjs/toolkit'
import storeReducer from './storeReducer'

export const reduxStore = configureStore({
  reducer: {
    store: storeReducer
  }
})
export type RootState = ReturnType<typeof reduxStore.getState>
export type AppDispatch = typeof reduxStore.dispatch

