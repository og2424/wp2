import { createSlice } from '@reduxjs/toolkit'
import { PayloadAction } from '@reduxjs/toolkit/dist/createAction';
import Store from '../models/storeModel'

export interface StoreReducerState {
    stores: Store[];
}
export const initialState: StoreReducerState = {
    stores: []
}

export const storeSlice = createSlice({
    name: 'store' ,
    initialState,
    reducers: {
        addStore: (state, action:PayloadAction<Store>) => {
            state.stores.push(action.payload);
        },
        removeStore: (state, action:PayloadAction<string>) => {
            state.stores = state.stores.filter((store) => 
                store.id !== action.payload
            )
        },
        setStore: (state, action:PayloadAction<Store[]>) => {
            state.stores = action.payload;
        }
    }

})

export const  { addStore, removeStore, setStore} = storeSlice.actions;

export default storeSlice.reducer;