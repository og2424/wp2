import React from 'react';
import '../css/home.css';

const Home = () => {
  return (
    <div className="home">
      <h1 className="home-title">Herzlich Willkommen</h1>
      <p className="home-description">Der einfachste Weg zum Einkaufen.</p>
    </div>
  );
}

export default Home;
