import React, { Component } from 'react';
import '../css/InputFields.css';

interface State {
  field1: string;
  field2: string;
  field3: string;
  field4: string;
  submitted: boolean;
}

class InputFields extends Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {
      field1: '',
      field2: '',
      field3: '',
      field4: '',
      submitted: false,
    };
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    this.setState({ [name]: value } as unknown as Pick<State, keyof State>);
  };

  handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    this.setState({ submitted: true });
    console.log('Data Submitted')
    // you can also do some async call to the server here
  };

  render() {
    const { field1, field2, field3, field4, submitted } = this.state;

    return (
      <div className="form-container">
        <form onSubmit={this.handleSubmit}>
          <h1>Nutzer erstellen</h1>
          <div className="form-group">
            <label>Vorname</label>
            <input
              className="form-control"
              name="field1"
              value={field1}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Nachname</label>
            <input
              className="form-control"
              name="field2"
              value={field2}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>E-mail</label>
            <input
              className="form-control"
              name="field3"
              value={field3}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Passwort</label>
            <input
              className="form-control"
              name="field4"
              value={field4}
              onChange={this.handleChange}
/>
</div>
<div>
<button type="submit" className="submit-button">Submit</button>
</div>
</form>
{submitted && (
<div className="form-result">
<p>Vorname {field1}</p>
<p>Nachname {field2}</p>
<p>E-mail {field3}</p>
<p>Passwort {field4}</p>
</div>
)}
</div>
);
}
}

export default InputFields;


