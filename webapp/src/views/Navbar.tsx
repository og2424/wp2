import React from 'react'
import { Link } from 'react-router-dom';
import '../css/Navbar.css';

const Navbar = () => {
  return (
    <nav>
        <Link to={'/'}>Home</Link>
        <Link to={'Stores'}>Geschäfte</Link>
        <Link to={'shoppingList'}>Einkaufszettel</Link>
        <Link to={'article'}>Artikel</Link>
        <Link to={'invoice'}>Rechnung</Link>
        <Link to={'servAgreement'}>Vereinbarung</Link>
        <Link to={'user'}>Nutzer</Link>

      
    </nav>
  )
}

export default Navbar
