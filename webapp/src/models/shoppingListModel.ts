export default interface shoppingList{
    name: string;
    Senior: string;
    Helper: string;
    shoppingTime: Date;
    price: number;
    articles: [{
        article:string;
        purchases:string;
    }];
}

