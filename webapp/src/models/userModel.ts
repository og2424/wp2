export default interface User {
    id : string;
    firstName: string;
    lastName: string;
    profiles: string;
    email: string;
    username: string;
    password: string;

}

