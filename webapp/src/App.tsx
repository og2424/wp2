import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import  storeService from './services/storeService';
import{Routes,Route} from 'react-router-dom';
import Store from './views/storeView';
import ShList from './views/shoppingList';
import Home from './views/Home';
import Navbar from './views/Navbar';
import Article from './views/article';
import Invoice from './views/invoice';
import ServAgreement from './views/servAgreementService';
import User from './views/user';


 function App() {
  


  return (
    <> 
    <Navbar />
      <Routes>
        <Route path= '/' element={<Home />}></Route>
        <Route path ='Stores' element={<Store />}></Route>
        <Route path ='shoppingList' element={<ShList />}></Route>
        <Route path ='article' element={<Article />}></Route>
        <Route path ='invoice' element={<Invoice />}></Route>
        <Route path ='servAgreement' element={<ServAgreement />}></Route>
        <Route path ='user' element={<User />}></Route>

    </Routes>      
    </>
  );
}

export default App;
