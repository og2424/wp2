import axios from 'axios';
import article from '../models/articleModel';

const header =  {"Content-Type": "application/json"}
const url = 'http://localhost:5000/api/';



async function createArticle(name:string,brand:string,description:string,size:string) {
    
        const promise = axios.post<article>(url + 'article',{
            brand: brand,
            name : name,
            description: description,
            size: size

          
        })
        const datapromise = promise.then(res => res.data)
        .catch(err => console.error(err));
        return datapromise;
}
async function getAllArticle() {
    const promise = axios.get<article>(url + 'article',{
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}
async function deleteAArticle(id:string) {
    const promise = axios.delete<article>(url + 'article' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function updateArticle(id:string, name:string,brand:string,description:string,size:string) {
    const promise = axios.patch<article>(url + 'article' +'/' + id, {
        brand: brand,
        name : name,
        description: description,
        size: size


    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




const shoppingListService = {createArticle,getAllArticle,deleteAArticle,updateArticle};



export default shoppingListService;



