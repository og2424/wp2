import axios from 'axios';
import list from '../models/shoppingListModel';

const header =  {"Content-Type": "application/json"}
const url = 'http://localhost:5000/api/';



async function createShoppingList(name:string,senior: string, helper: string, shoppingTime: Date, price: number,articles:object[]) {
    
        const promise = axios.post<list>(url + 'ShoppingList',{
            name : name,
            Senior: senior,
            Helper: helper,
            shoppingTime:shoppingTime,
            Price: price,
            articles :articles
        })
        const datapromise = promise.then(res => res.data)
        .catch(err => console.error(err));
        return datapromise;
}
async function getallLists() {
    const promise = axios.get<list>(url + 'ShoppingList',{
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}
async function deleteAList(id:string) {
    const promise = axios.delete<list>(url + 'ShoppingList' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function updateList(id:string, name:string,senior: string, helper: string, shoppingTime: Date, price: number,articles:object[]) {
    const promise = axios.patch<list>(url + 'ShoppingList' +'/' + id, {
        name : name,
        Senior: senior,
        Helper: helper,
        shoppingTime:shoppingTime,
        Price: price,
        articles :articles
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




const shoppingListService = {createShoppingList,getallLists,deleteAList,updateList};



export default shoppingListService;



