import axios from 'axios';
import agreement from '../models/servAgreementModel';

const header =  {"Content-Type": "application/json"}
const url = 'http://localhost:5000/api/';



async function createAgreement(Senior:string,Createdby:string,payment:string,deliveryTime:Date) {
    
        const promise = axios.post<agreement>(url + 'agreements',{
            senior: Senior,
            Createdby : Createdby,
            payment: payment,
            deliveryTime: deliveryTime

          
        })
        const datapromise = promise.then(res => res.data)
        .catch(err => console.error(err));
        return datapromise;
}
async function getAllAgreements() {
    const promise = axios.get<agreement>(url + 'agreements',{
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}
async function deleteAAgreement(id:string) {
    const promise = axios.delete<agreement>(url + 'agreements' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function updateAgreement(id:string, Senior:string,Createdby:string,payment:string,deliveryTime:Date) {
    const promise = axios.patch<agreement>(url + 'agreements' +'/' + id, {
        senior: Senior,
        Createdby : Createdby,
        payment: payment,
        deliveryTime: deliveryTime


    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




const shoppingListService = {updateAgreement,deleteAAgreement,createAgreement,getAllAgreements};



export default shoppingListService;



