import axios from 'axios';
import store from '../models/storeModel';

const url = 'http://localhost:5000/api/';

async function createStore(name:string) {
    
    // const promise = axios.post<store>(url + 'store',{
    //     storeName : name
    // })
    // const datapromise = data.then(res => res.data)
    // .catch(err => console.error(err));
    // return datapromise;
}
// function getAllStores() {
//     let datapromise: store[] = [];
//     const promise = axios.get<store[]>(url + 'store',{
//     })     
//     datapromise = promise.then(res => res.data)
//         console.log('G');
//     return datapromise;
// }
async function getAllStores() {
    let stores: store[] = [];
    const promise = axios.get<store[]>(url + 'store');
    await promise.then(res => {
        if (res.data instanceof Array) {
          stores = res.data;
        } 
      });
      return stores;

    
    // let datapromise: store[] = [];
    // const promise = axios.get<store[]>(url + 'store',{
    // })
    // const data: [] = promise.then(res => res.data)
    // .catch(err => console.error(err));
    // console.log(data);
    // return data;
    // return datapromise;
}
async function deleteAStore(id:string) {
    const promise = axios.delete<store>(url + 'store' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function updateStore(id:string, name:string) {
    const promise = axios.patch<store>(url + 'store' +'/' + id, {
        storeName : name
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




const storeServices = {createStore,getAllStores,deleteAStore,updateStore};



export default storeServices;



