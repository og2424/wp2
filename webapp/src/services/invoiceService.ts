import axios from 'axios';
import invoice from '../models/invoiceModel';

const header =  {"Content-Type": "application/json"}
const url = 'http://localhost:5000/api/';



async function createInvoice(ShoppingList: string, priceArticle: string, travelcost: string, Time: number) {
    
        const promise = axios.post<invoice>(url + 'invoice',{
            ShoppingList: ShoppingList,
            priceArticle: priceArticle,
            travelcost: travelcost,
            Time: Time

          
        })
        const datapromise = promise.then(res => res.data)
        .catch(err => console.error(err));
        return datapromise;
}
async function getAllInvoices() {
    const promise = axios.get<invoice>(url + 'invoice',{
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}
async function deleteAInvoice(id:string) {
    const promise = axios.delete<invoice>(url + 'invoice' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function UpdateInvoice(id:string, ShoppingList: string, priceArticle: string, travelcost: string, Time: number) {
    const promise = axios.patch<invoice>(url + 'invoice' +'/' + id, {
        ShoppingList: ShoppingList,
        priceArticle: priceArticle,
        travelcost: travelcost,
        Time: Time

      

    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




const shoppingListService = {createInvoice,getAllInvoices,deleteAInvoice,UpdateInvoice};



export default shoppingListService;



