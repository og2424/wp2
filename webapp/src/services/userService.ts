import axios from 'axios';
import user from '../models/userModel';

const header =  {"Content-Type": "application/json"}
const url = 'http://localhost:5000/api/';

async function createUser(firstName :string, lastName : string, profiles: string, email: string, username: string, password: string) {
    
        const promise = axios.post<user>(url + 'users',{
            firstName : firstName,
            lastName : lastName,
            profiles: profiles,
            email: email,
            username: username,
            password: password,

        })
        const datapromise = promise.then(res => res.data)
        .catch(err => console.error(err));
        return datapromise;
}
async function getAllusers() {
    const promise = axios.get<user>(url + 'user',{
    })
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}
async function deleteAUser(id:string) {
    const promise = axios.delete<user>(url + 'user' +'/' + id);
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}

async function updateUser(firstName :string, lastName : string, profiles: string, email: string, username: string, password: string,id:string) {
    const promise = axios.patch<user>(url + 'user' +'/' + id, {
        firstName : firstName,
        lastName : lastName,
        profiles: profiles,
        email: email,
        username: username,
        password: password,
})
    const datapromise = promise.then(res => res.data)
    .catch(err => console.error(err));
    return datapromise;
}




export default {createUser,getAllusers,deleteAUser,updateUser};



