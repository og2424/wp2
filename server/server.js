const { application } = require("express");
const express = require("express");
const {MongoClient} = require("mongodb");
const { default: mongoose, connect } = require("mongoose");
const UserRoutes = require('./routes/users');
const StoreRoutes = require('./routes/stores');
const ArticleRoutes = require('./routes/articles');
const ShoppingListRoute = require('./routes/ShoppingList');
const AgreementsRoute = require('./routes/agreement');
const InvoiceRoute = require('./routes/invoices');
const cors = require("cors");

const uri = "mongodb://127.0.0.1:27017/Database";
const server = express();

//middleware
server.use(express.json());
server.use(cors());
// server.use((req, res, next) => {
//     //res.setHeader('Access-Control-Allow-Origin' ,'*');
//     console.log(req.path, req.method)
//     next()
//   })

// routes !!! evtl. das direkt in routes reinpacken
server.use('/api/user', UserRoutes);
server.use('/api/store', StoreRoutes);
server.use('/api/article', ArticleRoutes);
server.use('/api/shoppingList', ShoppingListRoute);
server.use('/api/agreements', AgreementsRoute);
server.use('/api/invoice', InvoiceRoute);



//requests
mongoose.connect(uri)
    .then(() => {
        server.listen(5000,function() { 
        console.log("STARTED Server on Port 5000")
         });
    })
    .catch((error) => {
      console.log(error);
    });





















//   Funktion damit es verbunden wird und jetzt erstmal zeitweise werden Dokumente eingefügt.
// connect();
// async function connect() {
//     try {
//         const client = new MongoClient(uri);

//         await client.connect();
//         const db = client.db("Database");
//         const tester = db.collection("users");
//         const cursor = await tester.find();
//         // tester.insertMany([
//         //     {
//         //         "name" : "TEER"
//         //     },
//         //     {
//         //         "name" : "E"
//         //     }
//         // ]);
//     } catch (ex) {
//         console.error(ex);
//     }

// }

  
