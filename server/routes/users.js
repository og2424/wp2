const { Router } = require('express');
const express = require('express');
const {createNewUser, getAllUser,getUser,updateUser,deleteUser} = require('../controller/userController');

const router = express.Router();

//Get all User
router.get('/', getAllUser);

// GET a single User
router.get('/username', getUser);

// Create a new User
router.post('/', createNewUser);

// DELETE a User
router.delete('/:id', deleteUser)

// UPDATE a User
router.patch('/:id', updateUser)

module.exports = router
