const { Router } = require('express');
const express = require('express');
const {updateStore,deleteStore,createNewStore,getStore,getAllStores} = require('../controller/StoreController');

const router = express.Router();

//Get all User
router.get('/', getAllStores);
// GET a single User
router.get('/name', getStore);

// Create a new User
router.post('/', createNewStore);

// DELETE a User
router.delete('/:id', deleteStore);

// UPDATE a User
router.patch('/:id', updateStore);

module.exports = router
