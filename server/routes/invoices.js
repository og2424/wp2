const { Router } = require('express');
const express = require('express');
const {getAllInvoices,getInvoice,createNewInvoice,deleteInvoice,updateInvoice} = require ('../controller/InvoiceController')

const router = express.Router();

router.get('/', getAllInvoices);

router.get('/invoice', getInvoice);

router.post('/', createNewInvoice);

router.patch('/:id', updateInvoice);

router.delete('/:id', deleteInvoice);


module.exports = router;