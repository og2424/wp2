const { Router } = require('express');
const express = require('express');
const {updateAgreement,deleteAgreement,createAgreement,getAgreement,getAllAgreements} = require('../controller/ServiceAgreementController');

const router = express.Router();

//Get all Article
router.get('/', getAgreement);

// GET a single Article
router.get('/name', getAllAgreements);

// Create a new Article
router.post('/', createAgreement);

// DELETE a Article
router.delete('/:id', deleteAgreement);

// UPDATE a Article
router.patch('/:id', updateAgreement);

module.exports = router;