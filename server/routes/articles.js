const { Router } = require('express');
const express = require('express');
const {getAllArticle,getArticle,createNewArticle,deleteArticle,updateArticle} = require('../controller/ArticleController');

const router = express.Router();

//Get all Article
router.get('/', getAllArticle);

// GET a single Article
router.get('/name', getArticle);

// Create a new Article
router.post('/', createNewArticle);

// DELETE a Article
router.delete('/:id', deleteArticle);

// UPDATE a Article
router.patch('/:id', updateArticle);

module.exports = router;
