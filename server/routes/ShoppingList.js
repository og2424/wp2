const { Router } = require('express');
const express = require('express');
const {createShopList, getAllShoppingList, getShoppingList,updateShoppingList, deleteShoppingList} = require('../controller/ShoppingListController');

const router = express.Router();

router.get('/', getAllShoppingList);

router.get('/user', getShoppingList);

router.post('/', createShopList);

router.patch('/:id', updateShoppingList);

router.delete('/:id', deleteShoppingList);



module.exports = router;